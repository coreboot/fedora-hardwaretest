repo --name=buildhost --baseurl=http://%REPO%/rpms

%packages

# Exclude unwanted groups that fedora-live-base.ks pulls in
-@dial-up
-@input-methods
-@standard
-@multimedia
-@printing
-anaconda
-anaconda-install-env-deps
-anaconda-live
-@anaconda-tools
-sane-backends*
-libsane-hpaio
-simple-scan
-totem
-totem-nautilus
-tix

# Make sure to sync any additions / removals done here with
# workstation-product-environment in comps
@base-x
@core
@fonts
@gnome-desktop
@guest-desktop-agents
@hardware-support
@multimedia
@networkmanager-submodules

# Exclude unwanted packages from @anaconda-tools group
-gfs2-utils
-gvfs-gphoto2
-reiserfs-utils

# Exlude GUI applications
-libreoffice-*
-libreofficekit
-gnome-documents
-gnome-documents-libs
-gnome-photos
-gnome-online-miners
-gnome-getting-started-docs
-gnome-initial-setup
-gnome-font-viewer
-gnome-contacts
-gnome-calendar
-gnome-contacts
-gnome-maps
-gnome-weather
-gnome-todo
-gnome-user-docs
-gnome-user-share
-gnome-video-effects
-cheese
-gnome-boxes

# Remove other 
-httpd
-firewalld

# FIXME: replace with systemd
chkconfig

# For EFI boot
grub2-efi

# Useful tools
unzip
redhat-lsb-core
net-tools
tpm-tools
tpm2-tools
pciutils

# for entropy on pre Ivy platforms
haveged
rng-tools

# for LAVA
wget

%end

%post

# Load kernel modules
echo "coreboot-memconsole
coreboot-table
" > /etc/modules-load.d/google-firmware-drivers.conf

%end

firewall --disabled

network --bootproto=dhcp --device=eth0 --activate --onboot=on
services --enabled=NetworkManager,ModemManager,sshd


%post

cat >> /etc/rc.d/init.d/hardwaretest-custom << 'EOF'
#!/bin/bash
#
# live: Custom init script for hardwaretest image
#
# chkconfig: 345 99 01
# description: Custom init script for hardwaretest image.

# disable updates plugin
cat >> /usr/share/glib-2.0/schemas/org.gnome.software.gschema.override << FOE
[org.gnome.software]
download-updates=false
FOE

# disable version checking
gsettings set org.gnome.shell disable-extension-version-validation "true"

# rebuild schema cache with any overrides we installed
glib-compile-schemas /usr/share/glib-2.0/schemas

# Turn off PackageKit-command-not-found while uninstalled
if [ -f /etc/PackageKit/CommandNotFound.conf ]; then
  sed -i -e 's/^SoftwareSourceSearch=true/SoftwareSourceSearch=false/' /etc/PackageKit/CommandNotFound.conf
fi

# SSH setup
sed -i 's/.*UseDNS.*/UseDNS no/' /etc/ssh/sshd_config
mkdir -m 0700 -p /root/.ssh
cat > /root/.ssh/authorized_keys << EOKEYS
EOKEYS
chmod 600 /root/.ssh/authorized_keys
chown -R root: /root/.ssh/

echo "PermitRootLogin yes" >> /etc/ssh/sshd_config
echo "PermitEmptyPasswords yes" >> /etc/ssh/sshd_config

# Enable serial on all console=
# systemd does not enable all all consoles
for i in $(cat /proc/cmdline |tr " " "\n"|grep console=); do
# Set baud rate first. Fix for broken agetty seen on real hardware.
stty -F /dev/$(echo $i|cut -d'=' -f2 | cut -d',' -f1) $(echo $i|cut -d'=' -f2 | cut -d',' -f2|cut -d'n' -f1) &>/dev/null || /bin/true
systemctl start "serial-getty@$(echo $i|cut -d'=' -f2 | cut -d',' -f1)"
done

systemctl start sshd.service || /bin/true
systemctl start tcsd.service || /bin/true
EOF

chmod 755 /etc/rc.d/init.d/hardwaretest-custom
/sbin/restorecon /etc/rc.d/init.d/hardwaretest-custom
/sbin/chkconfig --add hardwaretest-custom

%end
