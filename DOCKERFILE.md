# Dockerfile

To build the Docker container use
```
DOCKER_BUILDKIT=1 docker build . -t fedora-hardwaretest
```

To run it:
```
docker run --device-cgroup-rule="b 7:* rmw" --privileged -it fedora-hardwaretest /bin/bash
```
