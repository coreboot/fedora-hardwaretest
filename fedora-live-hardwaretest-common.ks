# Maintained by the Fedora Workstation WG:
# http://fedoraproject.org/wiki/Workstation
# mailto:desktop@lists.fedoraproject.org

%include fedora-live-base.ks
%include fedora-hardwaretest-common.ks

%packages
# For syslinux:
fedora-logos
# for grub2
grub2-efi-x64-modules
grub2-efi-x64-cdboot
# Do NOT include grub2-efi or shim here!
# Available on Fedora 27+
# For 32bit EFI:
# shim-ia32
# For 64bit EFI:
shim-x64
efibootmgr
isomd5sum
syslinux

%end

#
# Disable this for now as packagekit is causing compose failures
# by leaving a gpg-agent around holding /dev/null open.
#
#include snippets/packagekit-cached-metadata.ks

# WARNING:
# for live image the EFI files must NOT be installed on a separate partition!
# part / --size 4096

%post

cat >> /etc/rc.d/init.d/livesys-custom << 'EOF'
#!/bin/bash
#
# live: Custom init script for live image
#
# chkconfig: 345 99 01
# description: Custom init script for live image.

# make the installer show up
if [ -f /usr/share/applications/liveinst.desktop ]; then
  # Show harddisk install in shell dash
  sed -i -e 's/NoDisplay=true/NoDisplay=false/' /usr/share/applications/liveinst.desktop ""
  # need to move it to anaconda.desktop to make shell happy
  mv /usr/share/applications/liveinst.desktop /usr/share/applications/anaconda.desktop

  cat >> /usr/share/glib-2.0/schemas/org.gnome.shell.gschema.override << FOE
[org.gnome.shell]
favorite-apps=['firefox.desktop', 'evolution.desktop', 'rhythmbox.desktop', 'shotwell.desktop', 'org.gnome.Nautilus.desktop', 'anaconda.desktop']
FOE

  # Make the welcome screen show up
  if [ -f /usr/share/anaconda/gnome/fedora-welcome.desktop ]; then
    mkdir -p ~liveuser/.config/autostart
    cp /usr/share/anaconda/gnome/fedora-welcome.desktop /usr/share/applications/
    cp /usr/share/anaconda/gnome/fedora-welcome.desktop ~liveuser/.config/autostart/
  fi

  # Copy Anaconda branding in place
  if [ -d /usr/share/lorax/product/usr/share/anaconda ]; then
    cp -a /usr/share/lorax/product/* /
  fi
fi

# make sure to set the right permissions and selinux contexts
chown -R liveuser:liveuser /home/liveuser/
restorecon -R /home/liveuser/

EOF

chmod 755 /etc/rc.d/init.d/livesys-custom
/sbin/restorecon /etc/rc.d/init.d/livesys-custom
/sbin/chkconfig --add livesys-custom

%end
