# Maintained by the Fedora Workstation WG:
# http://fedoraproject.org/wiki/Workstation
# mailto:desktop@lists.fedoraproject.org

# Only for ARM...
#%include fedora-disk-base.ks

%include fedora-disk-hardwaretest-base.ks
%include fedora-hardwaretest-common.ks
%include fedora-hardwaretest-minimal.ks
%include fedora-hardwaretest-buildserver.ks

%packages

# For syslinux:
fedora-logos
# For grub2
grub2-pc-modules
%end

zerombr
clearpart --all --initlabel --disklabel=msdos

bootloader --timeout=1 --append="earlyprintk=uart8250,io,0x2f8,57600n1 console=uart8250,io,0x2f8,57600n1 loglevel=7 cpuidle.off=1 iomem=relaxed no_timer_check verbose nokaslr audit=0 systemd.log_color=0 systemd.log_level=debug" --location=mbr
part / --size=8192 --fstype ext4
