# fedora-disk-base.ks
#
# Defines the basics for all kickstarts in the fedora-live branch
# Does not include package selection (other then mandatory)
# Does not include localization packages or configuration
#
# Does includes "default" language configuration (kickstarts including
# this template can override these settings)

#text: FIXME: Doesn't work with livemedia-creator
lang en_US.UTF-8
keyboard us
timezone US/Eastern
auth --useshadow --passalgo=sha512
selinux --enforcing
firewall --enabled --service=mdns
services --enabled=sshd,NetworkManager,chronyd
network --bootproto=dhcp --device=link --activate
rootpw --lock --iscrypted locked
shutdown

bootloader --timeout=1

zerombr
clearpart --all --initlabel --disklabel=msdos

# make sure that initial-setup runs and lets us do all the configuration bits
firstboot --reconfig

%include fedora-repo.ks

%packages
@base-x
@guest-desktop-agents
@standard
@core
@fonts
@input-methods
@dial-up
@multimedia
@hardware-support
@printing

# Without this, initramfs generation during live image creation fails: #1242586
dracut-live
grub2-efi
# Available on Fedora 27+
grub2-efi-cdboot
shim
# Available on Fedora 27+
shim-ia32
efibootmgr
isomd5sum
syslinux

# anaconda needs the locales available to run for different locales
glibc-all-langpacks
%end

%post

# add liveuser user with no passwd
action "Adding live user" useradd \$USERADDARGS -c "Live System User" liveuser
passwd -d liveuser > /dev/null
usermod -aG wheel liveuser > /dev/null

# Remove root password lock
passwd -d root > /dev/null

# Work around https://bugzilla.redhat.com/show_bug.cgi?id=1193590
cp /etc/skel/.bash* /var/roothome

echo -n "Network fixes"
# initscripts don't like this file to be missing.
cat > /etc/sysconfig/network << EOF
NETWORKING=yes
NOZEROCONF=yes
EOF

# make sure firstboot doesn't start
echo "RUN_FIRSTBOOT=NO" > /etc/sysconfig/firstboot

# Disable network service here, as doing it in the services line
# fails due to RHBZ #1369794
/sbin/chkconfig network off

# Anaconda is writing an /etc/resolv.conf from the install environment.
# The system should start out with an empty file, otherwise cloud-init
# will try to use this information and may error:
# https://bugs.launchpad.net/cloud-init/+bug/1670052
truncate -s 0 /etc/resolv.conf

mkdir -p /etc/dracut.conf.d/
echo filesystems+=overlay > /etc/dracut.conf.d/01-overlay.conf

mkdir -p usr/lib/dracut/modules.d/90overlay-root/

echo "#!/bin/bash

check() {
    # do not add modules if the kernel does not have overlayfs support
    [ -d /lib/modules/\$kernel/kernel/fs/overlayfs ] || return 1
}

depends() {
    # We do not depend on any modules - just some root
    return 0
}

# called by dracut
installkernel() {
    instmods overlay
}

install() {
    inst_hook pre-pivot 10 "\$moddir/overlay-mount.sh"
}" > /usr/lib/dracut/modules.d/90overlay-root/module-setup.sh
chmod +x /usr/lib/dracut/modules.d/90overlay-root/module-setup.sh

echo "#!/bin/sh

# make a read-only nfsroot writable by using overlayfs
# the nfsroot is already mounted to $NEWROOT
# add the parameter rootovl to the kernel, to activate this feature

. /lib/dracut-lib.sh

if ! getargbool 0 rootovl ; then
    return
fi

modprobe overlay

# a little bit tuning
mount -o remount,nolock,noatime \$NEWROOT

# Move root
mkdir -p /live/image
mount --bind \$NEWROOT /live/image
umount \$NEWROOT

# Create tmpfs
mkdir /cow
mount -n -t tmpfs -o mode=0755 tmpfs /cow
mkdir /cow/work /cow/rw

# Merge both to new Filesystem
mount -t overlay -o noatime,lowerdir=/live/image,upperdir=/cow/rw,workdir=/cow/work,default_permissions overlay \$NEWROOT

# Let filesystems survive pivot
mkdir -p \$NEWROOT/live/cow
mkdir -p \$NEWROOT/live/image
mount --bind /cow \$NEWROOT/live/cow
umount /cow
mount --bind /live/image \$NEWROOT/live/image
umount /live/image

# Update filesystem with custom configuration
rootovlcfg=\$(getargs rootovlcfg)
if [ -n "\$rootovlcfg" ]; then
    if [ -d \$NEWROOT/etc/rootovl/\$rootovlcfg ]; then
	cp -a \$NEWROOT/etc/rootovl/\$rootovlcfg/* \$NEWROOT
    fi
fi" > /usr/lib/dracut/modules.d/90overlay-root/overlay-mount.sh
chmod +x /usr/lib/dracut/modules.d/90overlay-root/overlay-mount.sh

# Add nvme driver to initramfs
mkdir -p /etc/dracut.conf.d/
echo add_drivers+=nvme > /etc/dracut.conf.d/0-nvme.conf

dracut --regenerate-all --force

%end
