# Maintained by the Fedora Workstation WG:
# http://fedoraproject.org/wiki/Workstation
# mailto:desktop@lists.fedoraproject.org

%include fedora-live-hardwaretest-common.ks

%post

cat >> /etc/rc.d/init.d/livesys-custom-gdm << 'EOF'
#!/bin/bash
#
# live: Custom init script for live image
#
# chkconfig: 345 99 01
# description: Custom init script for live image.

# prevent possible race conditions with livesys
sleep 1

# disable updates plugin
cat >> /usr/share/glib-2.0/schemas/org.gnome.software.gschema.override << FOE
[org.gnome.software]
download-updates=false
FOE

# disable version checking
gsettings set org.gnome.shell disable-extension-version-validation "true"

# don't run gnome-initial-setup
mkdir ~liveuser/.config
chown -R liveuser: ~liveuser/.config
touch ~liveuser/.config/gnome-initial-setup-done

# FIXME: !!!!
# FIXME: !!!!
# find out why X hasn't permission to access the DRM subsystem

echo "needs_root_rights = yes" >> /etc/X11/Xwrapper.config

#
# make the installer show up
if [ -f /usr/share/applications/liveinst.desktop ]; then
  # Show harddisk install in shell dash
  sed -i -e 's/NoDisplay=true/NoDisplay=false/' /usr/share/applications/liveinst.desktop ""
  # need to move it to anaconda.desktop to make shell happy
  mv /usr/share/applications/liveinst.desktop /usr/share/applications/anaconda.desktop

  cat >> /usr/share/glib-2.0/schemas/org.gnome.shell.gschema.override << FOE
[org.gnome.shell]
favorite-apps=['firefox.desktop', 'evolution.desktop', 'rhythmbox.desktop', 'shotwell.desktop', 'org.gnome.Nautilus.desktop', 'anaconda.desktop']
FOE

  # Make the welcome screen show up
  if [ -f /usr/share/anaconda/gnome/fedora-welcome.desktop ]; then
    mkdir -p ~liveuser/.config/autostart
    cp /usr/share/anaconda/gnome/fedora-welcome.desktop /usr/share/applications/
    cp /usr/share/anaconda/gnome/fedora-welcome.desktop ~liveuser/.config/autostart/
  fi

  # Copy Anaconda branding in place
  if [ -d /usr/share/lorax/product/usr/share/anaconda ]; then
    cp -a /usr/share/lorax/product/* /
  fi
fi

# rebuild schema cache with any overrides we installed
glib-compile-schemas /usr/share/glib-2.0/schemas

# set up auto-login
cat > /etc/gdm/custom.conf << FOE
[daemon]
AutomaticLoginEnable=True
AutomaticLogin=liveuser
FOE

EOF

chmod 755 /etc/rc.d/init.d/livesys-custom-gdm
/sbin/restorecon /etc/rc.d/init.d/livesys-custom-gdm
/sbin/chkconfig --add livesys-custom-gdm

%end
