# Maintained by the Fedora Workstation WG:
# http://fedoraproject.org/wiki/Workstation
# mailto:desktop@lists.fedoraproject.org

%packages

# Tools to compile firmware
xz-lzma-compat
git-lfs
docker-compose
docker
grubby
go
flex
bison
git
make
gcc-gnat
xz
bzip2
gcc
g++
ncurses-devel
wget
zlib-devel
sshpass
mysql-server
elfutils-libelf-devel
awscli
pciutils-devel
libusb-devel
%end

%post

cat >> /etc/rc.d/init.d/startup-contest << 'EOF'
#!/bin/bash
#
# live: Custom init script for ConTest
#
# chkconfig: 345 99 01
# description: Custom init script for ConTest.

systemctl start contest.service || /bin/true
systemctl start mariadb || /bin/true

EOF


mkdir -p /root/.ssh
cat >> /root/.ssh/config << 'EOF'

stricthostkeychecking=accept-new

EOF

mkdir -p /opt/contest

cat >> /opt/contest/contest_ssh.sh << 'EOF'
#!/bin/bash
# Seems some environments put in quotes,
# some put things after the repo name...
REPO="$(echo "${@: -1}" | fmt -1 | grep / |tail -1 | cut -f2 -d/ |tr -d "\'"|tr -d "\"")"

if [ -n "$REPO" ] && [ -e "~/.ssh/$REPO" ] ; then 
	exec ssh -i ~/.ssh/$REPO $*
else
	exec ssh -i ~/.ssh/$REPO $*
	exec ssh $*
fi

EOF

cat >> /opt/contest/contest_service << 'EOF'
#!/bin/bash
# live: Custom init script for contest
#
# chkconfig: 345 99 01
# description: Custom init script for contest setup
# We do assume that the private/public ssh keys are already there

FILE=/root/.ssh/id_rsa
DIRECTORY=/root/contest

if [ ! -f "/root/.init/init_done" ]; then
    echo "First Startup detected - Burn new coreboot image to disk"
    mkdir -p /root/.init/.cache/
    cd /root/.init/.cache/
    aws s3 cp s3://9esec-fb-assets/coreboot-deltalake-evt-seabios.rom coreboot.rom
    if [ $? -ne 0 ]; then { echo "Unable to download firmware" ; cd / ; rm -rf /root/.init/.cache ; exit 1; } fi
    git clone "https://review.coreboot.org/flashrom"
    if [ $? -ne 0 ]; then { echo "Unable to clone Flashrom" ; rm -rf /root/.init/.cache ; } else
        cd flashrom
        make
        ./flashrom -p internal:ich_spi_mode=hwseq -c "Opaque flash chip" --noverify-all --ifd -i bios -w /root/.init/.cache/coreboot.rom
        if [ $? -ne 0 ]; then { echo "Unable to clone Flashrom" ; } else
            touch /root/.init/init_done
            rm -R /root/.init/.cache
        fi
    fi
fi

echo "ConTest init."

if [ ! -f $FILE ]; then
    echo "Private ssh key does not exists"
fi

if [ -d $DIRECTORY ]; then
    echo "Directory already exists - lets see if we can pull new updates here"
    cd /root/contest
    git pull
else
    git clone --branch yv3 https://github.com/9elements/contest.git /root/contest
    if [ $? -ne 0 ]; then { echo "Cloning Contest Repo failed"; exit 1; } fi
fi

echo "Downloading some assets for Deltalake"

mkdir -p /root/contest/cmds/contest/assets
cd /root/contest/cmds/contest/assets
aws s3 cp s3://9esec-fb-assets/coreboot-deltalake-evt-seabios.rom .
if [ $? -ne 0 ]; then { echo "WARNING: Unable to download assets."; } fi
aws s3 cp s3://9esec-fb-assets/coreboot-deltalake-evt-dut-prov.rom .
if [ $? -ne 0 ]; then { echo "WARNING: Unable to download assets."; } fi

echo "Init MySQL Database"
cd /root/contest/docker/mysql
mysql -u root < initdb.sql
if [ $? -ne 0 ]; then { echo "Failed init database"; } fi

echo "Building ConTest... This might take a while."
cd /root/contest/cmds/contest/
go build .

if [ $? -ne 0 ]; then { echo "Building ConTest failed." ; exit 1; } fi     
echo "Try to run Contest now"

./contest

EOF

cat >> /etc/systemd/system/contest.service << 'EOF'

[Unit]
Description=ConTest
Wants=network-online.target
After=systemd-resolved.service

[Service]
ExecStart=/bin/bash /opt/contest/contest_service
RestartSec=120
Restart=always
Environment=HOME=/root
Environment=GIT_SSH_COMMAND=/opt/contest/contest_ssh.sh

EOF

chmod 755 /opt/contest/contest_service
chmod 755 /opt/contest/contest_ssh.sh

chmod 755 /etc/rc.d/init.d/startup-contest
/sbin/restorecon /etc/rc.d/init.d/startup-contest
/sbin/chkconfig --add startup-contest

%end
