import http.server
from socketserver import ThreadingMixIn
from http.server import SimpleHTTPRequestHandler, HTTPServer
import os
import _thread
import argparse
import time
import sys
import subprocess

PORT = 8080

class Server(http.server.SimpleHTTPRequestHandler):
  def do_GET(self):
    if "http://" in self.path:
      self.path = self.path.replace("http://", "")
      while self.path[0] != '/':
        self.path = self.path[1:]
      self.path = self.path[1:]

    return super(Server,self).do_GET()

class ThreadedHTTPServer(ThreadingMixIn, HTTPServer):
    """Handle requests in a separate thread."""

def serve_http():
  Handler = Server

  with ThreadedHTTPServer(("0.0.0.0", PORT), Handler) as httpd:
    httpd.serve_forever()

def check_pid(pid):
    """ Check For the existence of a unix pid. """
    try:
        os.kill(pid, 0)
    except OSError:
        return False
    else:
        return True

def main():

  parser = argparse.ArgumentParser()
  parser.add_argument('--waitpid', type=int, default=0)
  args = parser.parse_args()

  _thread.start_new_thread(serve_http,())

  while args.waitpid > 0 and check_pid(args.waitpid):
    time.sleep(1)

if __name__ == '__main__':
  main()
