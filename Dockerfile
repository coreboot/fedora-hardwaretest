FROM registry.fedoraproject.org/fedora:31

# Update Repo
RUN dnf update -y

# Install Dependencies
RUN dnf install -y createrepo lorax-lmc-novirt vim-minimal pykickstart \
anaconda-tui make lorax-templates-generic awscli python-PyDrive qemu wget \
openssh-clients edk2-ovmf java-base64 git vim

RUN mknod $(losetup -f) b 7 $(losetup -f|sed -e 's/\/dev\/loop//') || /bin/true

# Clone Repo into /root/fedora-hardwaretest
RUN git clone https://gitlab.com/coreboot/fedora-hardwaretest.git /root/fedora-hardwaretest
WORKDIR /root/fedora-hardwaretest

# Set build env
RUN echo "run-in-mock:=0" >> target.inc
RUN echo "install-in-qemu:=1" >> target.inc
RUN echo "make-disk:=1" >> target.inc
RUN echo "make-iso:=0" >> target.inc
RUN echo "enable-uefi:=0" >> target.inc
