%packages

# Install hardware test packages
fwts
acpica-tools
stress
lm_sensors
hddtemp
# Chipsec kernel module is broken and unmaintained. Drop it for now.
#chipsec
coreboot-utils
# for coreboot utils:
pciutils-libs
google-firmware-drivers-dkms
flashrom
memtester

# FIXME: replace with systemd
chkconfig

# For EFI boot
grub2-efi

# Useful tools
unzip
redhat-lsb-core
net-tools
tpm-tools
tpm2-tools
pciutils
sshpass

# for entropy on pre Ivy platforms
haveged
rng-tools

# for LAVA
wget

# for hardware tests
golang

%end

%post

# Load kernel modules
echo "coreboot-memconsole
coreboot-table
" > /etc/modules-load.d/google-firmware-drivers.conf

%end

firewall --disabled

network --bootproto=dhcp --device=eth0 --activate --onboot=on
services --enabled=NetworkManager,ModemManager,sshd

