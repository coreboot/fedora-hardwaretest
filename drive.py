from pydrive.auth import GoogleAuth
from pydrive.drive import GoogleDrive
import argparse

parser = argparse.ArgumentParser(description='Tool to upload files to gdrive')
parser.add_argument('--rpath', required=True,
                    help='Remote path, might contain one / to create a subfolder')
parser.add_argument('--lpath',required=True,
                    help='Local file path to upload')
parser.add_argument('--mimetype',required=True,
                    help='The mimetype')
parser.add_argument('--public',  action='store_true', default=False,
                    help='Make file public available')
parser.add_argument('--list', action='store_true', default=False,
                    help='List the folder contents after upload')
parser.add_argument('--description', default="",
                    help='List the folder contents after upload')
parser.add_argument('--auth',action='store_true', default=False,
                    help='Run the Oauth authenticator')
args = parser.parse_args()

gauth = GoogleAuth()
if args.auth:
 gauth.LocalWebserverAuth()

drive = GoogleDrive(gauth)

def FolderExists(a):
 if "/" in a:
  root = a.split("/")[0]
  a = a.split("/")[1]
 else:
  root = "root"

 file_list = drive.ListFile({'q': "'%s' in parents and mimeType='application/vnd.google-apps.folder'" % root}).GetList()
 for f in file_list:
  if a == f['title']:
   return True
 return False

def FileExists(a):
 if "/" in a:
  root = drive.ListFile({'q': "'root' in parents and mimeType='application/vnd.google-apps.folder' and title='%s'" % a.split("/")[0]}).GetList()[0]['id']
  a = a.split("/")[1]
 else:
  root = "root"

 try:
  file_list = drive.ListFile({'q': "'%s' in parents and not mimeType='application/vnd.google-apps.folder' and title='%s'" % (root, a)}).GetList()
  if len(file_list) > 0:
   return True
 except:
  pass
 return False

path = args.rpath
if "/" in path:
 if not FolderExists(path.split("/")[0]):
  print ("Creating folder '%s'" % path.split("/")[0])
  folder = drive.CreateFile({'title' : path.split("/")[0], 'mimeType' : 'application/vnd.google-apps.folder'})
  folder.Upload()
 else:
  folder = drive.ListFile({'q': "'root' in parents and mimeType='application/vnd.google-apps.folder' and title='%s'" % path.split("/")[0]}).GetList()[0]

 folderid = folder['id']
 title = path.split("/")[1]
else:
 folderid = "root"
 title = path

if FileExists(path):
 file = drive.ListFile({'q': "'%s' in parents and not mimeType='application/vnd.google-apps.folder' and title='%s'" % (folderid, title)}).GetList()[0]
 file['mimeType'] = args.mimetype
else:
 file = drive.CreateFile({"title": title, "parents": [{"kind": "drive#fileLink", "id": folderid}], 'mimeType' : args.mimetype})
file['description'] = args.description
file.SetContentFile(args.lpath)
print ("Uploading '%s' to '%s' ..." % (args.lpath, args.rpath))
file.Upload()
print ("Done")

if args.public:
 print ("Setting permission to public...")
 # Insert the permission.
 permission = file.InsertPermission({
                        'type': 'anyone',
                        'value': 'anyone',
                        'role': 'reader'})
 print (permission)

# Auto-iterate through all files that matches this query
if args.list:
 file_list = drive.ListFile({'q': "'root' in parents and mimeType='application/vnd.google-apps.folder'"}).GetList()
 for f in file_list:
  for i in drive.ListFile({'q': "'%s' in parents" % f['id']}).GetList():
   print(f['title'] + "/" + i['title'])
 file_list = drive.ListFile({'q': "'root' in parents and not mimeType='application/vnd.google-apps.folder'"}).GetList()
 for f in file_list:
  print(f['title'])

