
project-version:=32
project-name:=Fedora Hardwaretest Linux
project-title:=Fedora Hardwaretest Linux
project-volid:=Fedora-Hardwaretest-$(project-version)
project-arch:=x86_64

ts := $(shell date +"%s")
vcpus := $(shell nproc)

build-dir:=$(shell pwd)/build
result-dir:=$(shell pwd)/results/$(ts)
log-dir:=$(shell pwd)/log/$(ts)

make-iso:=1
make-disk:=0
make-pxe:=0

enable-uefi:=0
run-in-mock:=1
install-in-qemu:=0

lmc-fixup:=
arm-platform:=

# No optional args by default
livemedia-creator-args:=

# cache dir for required files
cache-dir:=$(shell pwd)/cache

include target.inc

ifeq ($(make-iso),1)
 ifeq ($(make-pxe),1)
  $(error Can't use make-iso and make-pxe at the same time)
 endif
 ifeq ($(make-disk),1)
  $(error Can't use make-iso and make-disk at the same time)
 endif
 ifeq ($(project-arch),arm)
  $(error Can't build ISO on arm)
 endif

 livemedia-creator-args+=--make-iso --iso-only --macboot
 ksprefix:=live
endif
ifeq ($(make-disk),1)
 ifeq ($(make-pxe),1)
  $(error Can't use make-iso and make-pxe at the same time)
 endif
 livemedia-creator-args+=--make-disk --image-name boot.img
 ifeq ($(project-arch),arm)
  ksprefix:=arm
 else ifeq ($(project-arch),aarch64)
  ksprefix:=aarch64
 else
  ksprefix:=disk
 endif
endif
ifeq ($(make-pxe),1)
 livemedia-creator-args+=--make-pxe-live
  ksprefix:=live
endif

ifeq ($(enable-uefi),1)
 livemedia-creator-args+=--virt-uefi --ovmf-path /usr/share/OVMF/
endif
ifeq ($(make-disk),1)
  ifeq ($(enable-uefi),1)
    ksprefix:=$(ksprefix)-uefi
  else
    ksprefix:=$(ksprefix)-mbr
  endif
endif

qemu-ram:=2048
free-mem:=$(shell free | awk 'NR==2 {print $$4}')
ifeq ($(shell test $(free-mem) -gt 4096000; echo $$?),0)
 qemu-ram:=4096
endif

# common arguments for livemedia-creator
livemedia-creator-args+=--volid=$(project-volid) --releasever $(project-version) --resultdir=$(result-dir) --logfile=$(log-dir)/.log --lorax-templates=$(build-dir)/lorax/

ifeq ($(install-in-qemu),1)
# Arguments seem to end in installed kernel cmdline....
# Remove nomodeset here...
livemedia-creator-args+=--arch $(project-arch) --kernel 'net.ifnames=0 biosdevname=0 selinux=0 rd.debug verbose module_blacklist=floppy' --iso=$(cache-dir)/netboot-$(project-version)-$(project-arch).iso --ram $(qemu-ram) --vcpus $(vcpus)
else
livemedia-creator-args+=--no-virt
endif

# Fedora 32 removed support for title
ifneq ($(shell livemedia-creator --help|grep title),)
livemedia-creator-args+=--title '$(project-title) $(subst build-,,$@)'
endif

ifeq ($(run-in-mock),1)
# Detect old-chroot obsolete
oldchrootObsolete:=$(shell mock --help|grep old-chroot|grep -i Obsoleted)
MOCKOPT:=
ifeq ($(oldchrootObsolete),)
MOCKOPT+=--old-chroot
else
MOCKOPT+=--isolation=simple
endif
# Detect enable-network
enableNetworkPresent:=$(shell mock --help|grep enable-network)
ifneq ($(enableNetworkPresent),)
MOCKOPT+=--enable-network
endif

# Fix path when run in MOCK
 livemedia-creator-args:=$(subst $(dir $(result-dir)),/results/,$(livemedia-creator-args))
 livemedia-creator-args:=$(subst $(build-dir),/build/,$(livemedia-creator-args))
 livemedia-creator-args:=$(subst $(log-dir),/log/,$(livemedia-creator-args))
 livemedia-creator-args:=$(subst $(cache-dir),/cache/,$(livemedia-creator-args))
# Run commands in mock
 mock-arch:=$(shell uname -m)
 mock-name:=fedora-$(project-version)-$(mock-arch)
 MOCKCMD:=mock -r $(mock-name)
 MOCKSHELL:=$(MOCKCMD) --shell $(MOCKOPT) '--plugin-option=bind_mount:dirs=[("$(dir $(result-dir))", "/results"), ("$(build-dir)", "/build"), ("$(log-dir)","/log"), ("$(cache-dir)","/cache")]'
 # fix for broken container loop-device setup
 lmc-fixup:=mknod $$(losetup -f) b 7 $$(losetup -f|sed -e 's/\/dev\/loop//');
 # fix to support KVM in qemu
 lmc-fixup+=mknod /dev/kvm c 10 232;
 # run the HTTP server in mock
 lmc-fixup+=(cd /build; python3 httpserver.py --waitpid $$$$ &);
else
# MOCKOPT:=
 MOCKCMD:=/bin/true || echo
 MOCKSHELL:=eval
endif

help: all
all:
	@echo "Usage:"
	@echo "make build-gnome           - Build regular image"
	@echo "make build-minimal         - Build minimal image"
	@echo "make keys                  - Create new RSA key pair in ./keys"
	@echo "make docker (experimental) - Build inside docker"
	@echo ""
	@echo "Modify target.inc to change build target or environment"

$(shell mkdir -p $(build-dir))
$(shell mkdir -p $(dir $(result-dir)))
$(shell mkdir -p $(log-dir))
$(shell mkdir -p $(cache-dir))

# SSH keys
keys: keys/id_rsa keys/id_rsa.pub

keys/id_rsa keys/id_rsa.pub:
	ssh-keygen -b 4096 -t rsa -f ./keys/id_rsa -C "Hardwaretest insecure key" -N ""

# static kickstarts
KICKSTARTS:=$(addprefix $(build-dir)/,$(wildcard *.ks))

fedora-hardwaretest-common: fedora-hardwaretest-common.ks.tmpl keys/id_rsa.pub
ifeq ($(install-in-qemu),1)
	cat $< | sed -e "s|%REPO%|10.0.2.2:8080|g" | sed -e "s|^EOKEYS|$$(cat ./keys/id_rsa.pub)\nEOKEYS|g" > $(build-dir)/fedora-hardwaretest-common.ks
else
	cat $< | sed -e "s|%REPO%|127.0.0.1:8080|g" | sed -e "s|^EOKEYS|$$(cat ./keys/id_rsa.pub)\nEOKEYS|g" > $(build-dir)/fedora-hardwaretest-common.ks
endif

$(build-dir)/fedora-hardwaretest-common.ks: fedora-hardwaretest-common

# For qemu we need to flatten the kickstart files into one file
# Assume pykickstart is installed on the host...
$(build-dir)/fedora-flat-$(ksprefix)-%.ks: $(build-dir)/fedora-$(ksprefix)-hardwaretest-%.ks $(build-dir)/fedora-hardwaretest-common.ks $(KICKSTARTS)
	cd $(build-dir); ksflatten --config $< -o $@ --version F$(project-version)

$(KICKSTARTS): $(build-dir)/%.ks: %.ks
	cp $< $@

# Clean the mock and install packages
mock-clean: $(build-dir)/.mock-clean

$(build-dir)/.mock-clean:
	$(MOCKCMD) --init
	$(MOCKCMD) --install createrepo libblockdev libblockdev-* lorax lorax-lmc-novirt pykickstart anaconda-tui lorax-templates-generic qemu edk2-ovmf python3
	touch $@

mock-final:
	rm $(build-dir)/.mock-clean

# For qemu we need netinstall iso
$(cache-dir)/netboot-27-x86_64.iso:
	wget https://dl.fedoraproject.org/pub/fedora/linux/releases/27/Workstation/x86_64/iso/Fedora-Workstation-netinst-x86_64-27-1.6.iso -O $@
$(cache-dir)/netboot-27-aarch64.iso:
	wget https://dl.fedoraproject.org/pub/fedora/linux/releases/28/Server/aarch64/iso/Fedora-Server-netinst-aarch64-28-1.1.iso -O $@
$(cache-dir)/netboot-28-x86_64.iso:
	wget https://download.fedoraproject.org/pub/fedora/linux/releases/28/Workstation/x86_64/iso/Fedora-Workstation-netinst-x86_64-28-1.1.iso -O $@
$(cache-dir)/netboot-30-x86_64.iso:
	wget https://download.fedoraproject.org/pub/fedora/linux/releases/30/Workstation/x86_64/iso/Fedora-Workstation-netinst-x86_64-30-1.2.iso -O $@
$(cache-dir)/netboot-31-x86_64.iso:
	wget http://fedora.inode.at/releases/31/Everything/x86_64/iso/Fedora-Everything-netinst-x86_64-31-1.9.iso -O $@
$(cache-dir)/netboot-32-x86_64.iso:
	wget http://fedora.inode.at/releases/32/Everything/x86_64/iso/Fedora-Everything-netinst-x86_64-32-1.6.iso -O $@

ifeq ($(install-in-qemu),1)
build-%: $(build-dir)/fedora-flat-$(ksprefix)-%.ks $(cache-dir)/netboot-$(project-version)-$(project-arch).iso
else
build-%: $(build-dir)/fedora-flat-$(ksprefix)-%.ks
endif
	echo "Updating repo cache..."
	createrepo --database rpms/
	echo "Installing lorax files..."
	rm -rf $(build-dir)/lorax
	cp -r lorax $(build-dir)/
ifeq ($(run-in-mock),1)
	$(MAKE) mock-clean
endif
ifeq ($(enable-uefi),1)
	$(MOCKSHELL) "dd if=/dev/zero of=/usr/share/OVMF/OVMF_VARS.fd bs=1KiB count=128"
endif
ifeq ($(make-pxe),1)
	$(MOCKSHELL) "if [ -f /usr/lib/python3.7/site-packages/pylorax/creator.py ]; then sed -i \"s/dracut_args = DRACUT_DEFAULT/dracut_args = DRACUT_DEFAULT + ['--install', 'basename']/\" /usr/lib/python3.7/site-packages/pylorax/creator.py; fi"
	$(MOCKSHELL) "if [ -f /usr/lib/python3.8/site-packages/pylorax/creator.py ]; then sed -i \"s/dracut_args = DRACUT_DEFAULT/dracut_args = DRACUT_DEFAULT + ['--install', 'basename']/\" /usr/lib/python3.8/site-packages/pylorax/creator.py; fi"
endif
ifeq ($(run-in-mock),1)
ifeq ($(install-in-qemu),0)
	$(MOCKSHELL) -- "rm /var/run/anaconda.pid || /bin/true"
endif
	cp httpserver.py $(build-dir)/
	rm -rf $(build-dir)/rpms/
	cp -r rpms $(build-dir)/
	$(MOCKSHELL) -- "$(lmc-fixup) livemedia-creator $(livemedia-creator-args) --project '$(project-name) $(subst mock-,,$@)' --ks /build/$(<F) || (sleep 10; cat $(log-dir)/.log && cat $(log-dir)/virt-install.lo && /bin/false)"
else
	(python3 httpserver.py --waitpid $$$$ &); $(MOCKSHELL) -- "$(lmc-fixup) livemedia-creator $(livemedia-creator-args) --project '$(project-name) $(subst mock-,,$@)' --ks $< || (sleep 10; cat $(log-dir)/.log && cat $(log-dir)/virt-install.log && /bin/false)"
endif
ifeq ($(run-in-mock),1)
	$(MAKE) mock-final
endif

docker: *.ks fedora-live-hardwaretest.ks
	echo "FIXME: Doesn't work yet...."

	mkdir -p $(PWD)/results
	mkdir -p $(PWD)/logs
	docker run -t -i --security-opt=apparmor:unconfined --cap-add=SYS_ADMIN --device=/dev/loop0 --device=/dev/loop-control -v "$(PWD)/":/app/:Z fedora:$(project-version) /bin/bash -c " \
	dnf install -y lorax-templates-generic lorax-lmc-novirt pykickstart anaconda-tui; \
	sed -i \"s/replace @ROOT@ '/replace @ROOT@ 'audit=0 iomem=relaxed /\" /usr/share/lorax/templates.d/99-generic/live/*.tmpl; \
	livemedia-creator --make-iso --volid=$(project-volid) --releasever $(project-version)  --project \"$(project-name)\" --no-virt --resultdir=/app/results/$(ts) --logfile=/app/logs/$(ts)/$(ts).log --ks /app/fedora-live-hardwaretest.ks; \
	echo \"Done.\" \
	"

.PHONY: keys docker mock-clean fedora-hardwaretest-common
