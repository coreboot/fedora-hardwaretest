# Maintained by the Fedora Workstation WG:
# http://fedoraproject.org/wiki/Workstation
# mailto:desktop@lists.fedoraproject.org

%packages

# Exclude unwanted groups that fedora-hardwaretest-common.ks pulls in
-@gnome-desktop
-@fonts
-@multimedia
-@guest-desktop-agents
-gnome-online-accounts
-libwayland-*
-libwacom*
-libinput
-harfbuzz
-evolution-data-server
-pango
-cairo
-glx-utils
-xcb-util
-mtdev
-mcpp
-xorg-x11-*
-mesa*
# No need for graphical boot screen:
-plymouth*
-mpage
-hplip
-isdn4k-utils
-xsane
-xsane-gimp
-sane-backends

%end
