# Maintained by the Fedora Workstation WG:
# http://fedoraproject.org/wiki/Workstation
# mailto:desktop@lists.fedoraproject.org

# Only for ARM...
#%include fedora-disk-base.ks

%include fedora-disk-hardwaretest-base.ks
%include fedora-hardwaretest-common.ks
%include fedora-hardwaretest-minimal.ks
%include fedora-hardwaretest-tools.ks

%packages

# For syslinux:
fedora-logos
# For grub2
grub2-efi-x64-modules

%end

clearpart --all --initlabel --disklabel=gpt
zerombr

bootloader --timeout=1 --append="iomem=relaxed no_timer_check verbose nokaslr audit=0 earlyprintk=serial,ttyS0,115200 console=ttyS1,115200n8 console=ttyS0,115200n8 console=ttyS4,115200n8 systemd.log_color=0 systemd.log_level=debug rootovl" --location=boot
part / --size=3584 --fstype ext4
part /boot/efi --fstype="efi" --size=64
part /boot --size=448 --fstype ext4
